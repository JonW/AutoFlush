#!/bin/bash

#wget -O - https://gitlab.com/JonW/AutoFlush/raw/master/install.sh | bash

DIRECTORY="/usr/local/bin/AutoFlush"
systemctl stop AutoFlush.service

if [ -d "$DIRECTORY" ]; then
	echo "Upgrade AutoFlush the latest version"
	git -C $DIRECTORY fetch --all
	git -C $DIRECTORY reset --hard origin/master

else
	echo "Install AutoFlush"
	mkdir $DIRECTORY
	chmod 710 $DIRECTORY
	git -C $DIRECTORY clone https://gitlab.com/JonW/AutoFlush.git .
fi

cp $DIRECTORY/AutoFlush.service /lib/systemd/system/AutoFlush.service

systemctl daemon-reload
systemctl enable AutoFlush.service
systemctl start AutoFlush.service