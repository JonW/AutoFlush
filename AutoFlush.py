#!/usr/bin/env python3

import logging
import RPi.GPIO as GPIO #import the RPi.GPIO library
#import gpio #pip3 install gpio
import time #import the time library
import signal
import schedule           #pip3 install schedule / https://github.com/dbader/schedule
import datetime

logging.basicConfig(filename='/var/log/AutoFlush.log',
                    level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s: %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')

def PrintAndLog(LogString):
    print(str(LogString))
    logging.debug(str(LogString))

def term(signal,frame):
    PrintAndLog ("Ctrl+C captured, ending read.")
    continue_loop = False
    schedule.clear()
    Flusher11.Stop()
    Flusher13.Stop()
    GPIO.cleanup()
    PrintAndLog ("Goodbuy")

def PeekJob():
    PrintAndLog("Peek Job Start")
    Flusher11.Flush(PeekDuration)
    Flusher13.Flush(PeekDuration)
    PrintAndLog("Peek Job Finish")

def OffPeekJob():
    PrintAndLog("Off-Peek Job Start")
    Flusher11.Flush(OffPeekDuration)
    Flusher13.Flush(OffPeekDuration)
    PrintAndLog("Off-Peek Job Finish")

class Flusher(object):
    def __init__(self,pin):
        GPIO.setmode(GPIO.BOARD)
        self.relay = pin
        try:
            GPIO.setup(self.relay, GPIO.OUT)
        except:
            PrintAndLog("Flusher Failure on pin "+str(self.relay))
            exit(10)
        self.Stop()
        PrintAndLog("Flusher Ready on pin "+str(self.relay))
        
    def __del__(self):
        class_name = self.__class__.__name__
        PrintAndLog (class_name, "finished")
        
    def Start(self):
        PrintAndLog("Open Valve on pin "+str(self.relay))
        GPIO.output(self.relay, False)#Relay On

    def Stop(self):
        PrintAndLog("Close Valve on pin "+str(self.relay))
        GPIO.output(self.relay, True)#Relay Off

    def Flush(self, duration):
        PrintAndLog("Start Flush on pin "+str(self.relay))
        self.Start()
        PrintAndLog("Flush for "+str(duration)+" seconds.")
        time.sleep(duration)        #sleep for the duration of the flush
        self.Stop()

if __name__ == "__main__":
    
    # Hook the SIGINT
    signal.signal(signal.SIGINT, term)
    signal.signal(signal.SIGTERM, term)

    #CONFIG
    Peek = None
    continue_loop = True

    #Flush Peek
    PeekDays = [0,1,2,3,4] #Monday = 0
    PeekStart = datetime.time(7,0)
    PeekEnd = datetime.time(16,30)

    #Rate of Flush
    PeekFlush = 15
    OffPeekFlsuh = 45
    SensorFlushCount = 25 #Number of sensor activations to start a flush

    #Flush Duration
    PeekDuration = 30
    OffPeekDuration = 30

    Flusher11 = Flusher(11)
    Flusher13 = Flusher(13)

    while continue_loop:
        CurrentDay = datetime.datetime.today().weekday()
        CurrentTime = datetime.datetime.now().time()
        #Detect Peek Period
        if((CurrentDay in PeekDays) and (PeekStart <= CurrentTime <= PeekEnd)):
            if(Peek is False or Peek is None):
                PrintAndLog("Set Peek flush schedule")
                if(Peek is False):
                    PrintAndLog("Remove Off-Peek Schedule")
                    schedule.cancel_job(OffPeekSchedule)
                PeekSchedule = schedule.every(PeekFlush).minutes.do(PeekJob)
                #PeekSchedule = schedule.every(PeekFlush).seconds.do(PeekJob)
            Peek = True
        else:
            if(Peek is True or Peek is None):
                PrintAndLog("Set Off-Peek flush schedule")
                if(Peek is True):
                    PrintAndLog("Remove Peek Schedule")
                    schedule.cancel_job(PeekSchedule)
                OffPeekSchedule = schedule.every(OffPeekFlsuh).minutes.do(OffPeekJob)
                #OffPeekSchedule = schedule.every(OffPeekFlsuh).seconds.do(OffPeekJob)
            Peek = False

        schedule.run_pending()
        #print('.', end="")
        time.sleep(1)

        